import json

input_file = open('hotspot.config', 'w')
data = json.load(open('input.json'))["simulator"][0]

for k, v in data.items():
  if isinstance(v, dict):
    for k,v in v.items():
      input_file.write(k.upper()+"="+str(v)+"\n")
  else:
    input_file.write(k.upper()+"="+str(v)+"\n")
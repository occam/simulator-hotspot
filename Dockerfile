# DOCKER-VERSION 0.11.1
FROM occam/mjqxgzi--ovrhk3tuouwweyltmuwtembrgqwtanq-
ADD . /simulator
RUN apt-get update
RUN cd /simulator; mkdir package; wget http://lava.cs.virginia.edu/HotSpot/grab/HotSpot-5.02.tar.gz -O package/package.tar; cd package; tar xvf package.tar
RUN cd /simulator; /bin/bash -c 'source /occam/setup_python.sh; source /occam/setup_perl.sh; cd /simulator; bash /simulator/build.sh'
CMD ['/bin/bash']
